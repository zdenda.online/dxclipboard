package cz.d1x.dxclipboard;

import cz.d1x.dxclipboard.controller.Controller;
import cz.d1x.dxclipboard.ui.MainFrame;

import javax.swing.*;

/**
 * Main class of the application.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            // it's ok - will simply fall back to default
        }

        Controller controller = new Controller();
        MainFrame frame = new MainFrame(controller);
        frame.init();
        frame.setVisible(true);
    }
}
