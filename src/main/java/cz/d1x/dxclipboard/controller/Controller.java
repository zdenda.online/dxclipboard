package cz.d1x.dxclipboard.controller;

import cz.d1x.dxclipboard.logic.capture.ClipboardCapture;
import cz.d1x.dxclipboard.logic.capture.OwnerClipboardCapture;
import cz.d1x.dxclipboard.logic.output.ClipboardOutputExport;
import cz.d1x.dxclipboard.logic.capture.EntryListener;
import cz.d1x.dxclipboard.logic.capture.OutputBuilder;
import cz.d1x.dxclipboard.logic.output.OutputExport;

/**
 * Controller is facade that reacts to events from UI and is responsible for calling proper implementations.
 */
public class Controller {

    private OutputBuilder outputBuilder = new OutputBuilder();
    private ClipboardCapture capture = new OwnerClipboardCapture(outputBuilder);
    private OutputExport export = new ClipboardOutputExport();

    /**
     * Adds a new entry listener if there are any changes in captured entries.
     *
     * @param listener listener to be added
     */
    public void addEntryListener(EntryListener listener) {
        outputBuilder.addEntryListener(listener);
    }

    /**
     * Starts capturing clipboard.
     */
    public void startCapturing() {
        capture.start();
    }

    /**
     * Stops capturing clipboard.
     */
    public void stopCapturing() {
        capture.stop();
    }

    /**
     * Clears all currently added entries.
     */
    public void clearEntries() {
        outputBuilder.clearEntries();
    }

    /**
     * Exports captured contents using given delimiter and params for export.
     *
     * @param delimiter delimiter for final output
     * @param params    parameters for export
     */
    public void export(String delimiter, String... params) {
        outputBuilder.setDelimiter(delimiter);
        String output = outputBuilder.build();
        export.export(output, params);
    }
}
