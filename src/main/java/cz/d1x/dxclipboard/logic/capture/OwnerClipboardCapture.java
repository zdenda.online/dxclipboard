package cz.d1x.dxclipboard.logic.capture;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Capture implementation that uses clipboard ownership for capturing new clipboard contents.
 * It takes owner of system clipboard and when other application uses clipboard, this class notices it, gets current
 * content of clipboard and regains ownership again.
 * <p>
 * It passes captured contents into {@link OutputBuilder}.
 * Note that this class is thread- safe.
 */
public class OwnerClipboardCapture implements ClipboardCapture {

    private final Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    private final OwnerImpl owner = new OwnerImpl();
    private final AtomicBoolean enabled = new AtomicBoolean(false);
    private final OutputBuilder builder;

    public OwnerClipboardCapture(OutputBuilder builder) {
        this.builder = builder;
    }

    @Override
    public void start() {
        enabled.set(true);
        Transferable trans = systemClipboard.getContents(this);
        owner.regainOwnership(trans);
    }

    @Override
    public void stop() {
        enabled.set(false);
    }


    private final class OwnerImpl implements ClipboardOwner {

        @Override
        public void lostOwnership(Clipboard clipboard, Transferable contents) {
            if (!enabled.get()) return;
            try {
                Thread.sleep(200);  // larger elements may take some time to load
            } catch (InterruptedException e) {
                // simply ignore it
            }
            Transferable actualContents = clipboard.getContents(this);

            if (actualContents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    String stringContents = actualContents.getTransferData(DataFlavor.stringFlavor).toString();
                    builder.addStringEntry(stringContents);
                } catch (UnsupportedFlavorException | IOException e) {
                    // simply ignore it
                }
            }
            regainOwnership(actualContents);
        }

        void regainOwnership(Transferable transferable) {
            systemClipboard.setContents(transferable, this);
        }
    }
}
