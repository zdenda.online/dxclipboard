package cz.d1x.dxclipboard.logic.output;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Implementation that exports output back to clipboard contents.
 */
public class ClipboardOutputExport implements OutputExport {

    private final Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

    @Override
    public void export(String output, String... params) {
        StringSelection selection = new StringSelection(output);
        systemClipboard.setContents(selection, selection);
    }
}
