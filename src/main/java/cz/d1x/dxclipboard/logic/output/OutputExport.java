package cz.d1x.dxclipboard.logic.output;

/**
 * Interface that exports output to implementation-specific destination.
 */
public interface OutputExport {

    /**
     * Exports given output.
     *
     * @param output output to be exported
     * @param params parameters for export
     */
    void export(String output, String... params);
}
