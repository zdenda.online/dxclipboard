package cz.d1x.dxclipboard.ui;

import cz.d1x.dxclipboard.controller.Controller;

import javax.swing.*;
import java.awt.*;

/**
 * Main frame of the application.
 */
public class MainFrame extends JFrame {

    private static final int MARGIN = 10;
    private static final int WIDTH = 320;
    private static final int HEIGHT = 285;
    private static final Font HELP_FONT = new Font("Serif", Font.PLAIN, 12);

    private final Controller controller;
    private final JTextField delimiterField = new JTextField("\\n");

    public MainFrame(Controller controller) throws HeadlessException {
        super("DXClipboard");
        this.controller = controller;
    }

    public void init() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(false);
        setResizable(false);
        setLocationRelativeTo(null);
        setLayout(bLayout());

        add(capturePanel(), BorderLayout.NORTH);
        add(exportPanel(), BorderLayout.CENTER);
        add(settingsPanel(), BorderLayout.SOUTH);
    }

    private JComponent capturePanel() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Capture"));
        panel.setLayout(bLayout());

        JButton startButton = new JButton("START");
        JButton stopButton = new JButton("STOP");
        stopButton.setEnabled(false);

        startButton.addActionListener(e -> {
            startButton.setEnabled(false);
            stopButton.setEnabled(true);
            controller.startCapturing();
        });
        stopButton.addActionListener(e -> {
            controller.stopCapturing();
            stopButton.setEnabled(false);
            startButton.setEnabled(true);
        });

        JPanel entriesPanel = new JPanel(bLayout());
        JLabel entriesCountLabel = new JLabel("Count of entries: 0");
        controller.addEntryListener(ob -> entriesCountLabel.setText("Count of entries: " + ob.getEntriesCount()));
        JButton clearButton = new JButton("CLEAR");
        clearButton.addActionListener(e -> controller.clearEntries());
        entriesPanel.add(entriesCountLabel, BorderLayout.CENTER);
        entriesPanel.add(clearButton, BorderLayout.EAST);

        panel.add(startButton, BorderLayout.CENTER);
        panel.add(stopButton, BorderLayout.EAST);
        panel.add(entriesPanel, BorderLayout.SOUTH);
        return panel;
    }

    private JComponent exportPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Aggregate"));
        panel.setLayout(bLayout());

        JLabel helpLabel = new JLabel("Delimiter can contain: \\n = new line, \\t = tab");
        helpLabel.setFont(HELP_FONT);

        JButton exportButton = new JButton("AGGREGATE TO CLIPBOARD");
        String delimiter = delimiterField.getText()
                .replace("\\n", "\n")
                .replace("\\t", "\t");
        exportButton.addActionListener(e -> controller.export(delimiter));

        JPanel delimiterPanel = new JPanel();
        delimiterPanel.setLayout(bLayout());
        delimiterPanel.add(new JLabel("Delimiter: "), BorderLayout.WEST);
        delimiterPanel.add(delimiterField, BorderLayout.CENTER);
        delimiterPanel.add(helpLabel, BorderLayout.SOUTH);

        panel.add(delimiterPanel, BorderLayout.CENTER);
        panel.add(exportButton, BorderLayout.SOUTH);
        return panel;
    }

    private JPanel settingsPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Settings"));
        panel.setLayout(bLayout());

        JCheckBox checkBox = new JCheckBox("Always on top");
        checkBox.addActionListener(l -> MainFrame.this.setAlwaysOnTop(checkBox.isSelected()));

        panel.add(checkBox, BorderLayout.CENTER);
        return panel;
    }

    private BorderLayout bLayout() {
        return new BorderLayout(MARGIN, MARGIN);
    }

}
